<?php
$ts_mycnf = parse_ini_file("/data/project/basebot/replica.my.cnf");
// Create connection
$ukwiki_p = new mysqli("ukwiki.labsdb", $ts_mycnf['user'], $ts_mycnf['password'], "ukwiki_p");
// Check connection
if ($ukwiki_p->connect_error) {
    die("Connection to DB failed: " . $ukwiki_p->connect_error);
}
echo 'connected to DB at the least<br/>';
$sql = "select count(*) as number from `user` where user_editcount > 0";
$result = $ukwiki_p->query($sql);
$row = $result->fetch_assoc();
$answer = $row["number"];
$ukwiki_p->close(); // we don't need the connection itself anymore

include_once ( 'botclasses.php' );
$wiki = new wikipedia("https://uk.wikipedia.org/w/api.php");
$wiki->setUserAgent('User-Agent: BaseBot (http://meta.wikimedia.org/wiki/User:BaseBot)');
$wiki_mycnf = parse_ini_file("/data/project/basebot/wiki.my.cnf");
$wiki->login($wiki_mycnf['user'], $wiki_mycnf['password']);
$wiki->edit("Template:NUMBEROFACTIVEUSERSAGGR", "{{formatnum:". $answer . "}}<noinclude>\n{{doc}}\n</noinclude>");
